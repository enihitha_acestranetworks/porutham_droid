angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('PlaylistsCtrl', function($scope, service,$state, $ionicSlideBoxDelegate, $ionicGesture) {
  var obj = {};
  obj.token = 9791047476;
rasis = [];
$scope.match_result = {};
bride_name = '';
groom_name = '';
// $scope.match_result.girlraasi = "hai";
 service.post(url + 'getRaasis', obj).then(function(result) {
  // console.log(result);
  rasis = result;
        $scope.rasi = result;
    }, function(error) {
      if (error == 500 || error == 404 || error == 0) {
        $scope.news_nodata = true;
        $scope.news_loading_icon = false;
        $scope.news_loadmore = false;
        // $cordovaToast.showLongBottom('Network Failed' + error);
      }
    });

$scope.selectstar= function(id, type)
{

 var obj1 = {};
  obj1.token = 9791047476;
obj1.raasi_id=id;

 service.post(url + 'getStars', obj1).then(function(result) {
  console.log(result);
    if(type=='bride')
    {
        $scope.bride_stars = result;
    }
    else if(type=='groom')
    {
       $scope.groom_stars = result;
    }
console.log(rasis);

    }, function(error) {
      if (error == 500 || error == 404 || error == 0) {
        $scope.news_nodata = true;
        $scope.news_loading_icon = false;
        $scope.news_loadmore = false;
      }
    });
}

$scope.match = function(user)
{
bride_name = user.bride_name;
groom_name = user.groom_name;
  console.log(user);
  var obj2={};
  obj2.token =9791047476;
  obj2.girl_star =user.bride_star;
  obj2.girl_raasi ='';
  obj2.boy_raasi ='';
  obj2.boy_star =user.groom_star;
console.log(rasis);
angular.forEach(rasis,function(value,key){
  if(value.id == user.bride_rasi){
    console.log(value.name);
  obj2.girl_raasi = value.name;

  }

})
angular.forEach(rasis,function(values,key){
if(values.id == user.groom_rasi){
    console.log(values.name);

  obj2.boy_raasi = values.name;

  }
})
  console.log(obj2);

  service.post(url + 'getkalyanaporuttham', obj2).then(function(result) {
console.log(result);
if (result.status == 'success') {
   
    $state.go('match_result', {obj:result.match,bride_name:bride_name,groom_name:groom_name});
     
}
  })
}

   $scope.nextSlide = function() {
    $ionicSlideBoxDelegate.next();
  };
})

.controller('PlaylistCtrl', function($scope, $stateParams, $timeout) {

   $scope.doRefresh = function() {
    
    console.log('Refreshing!');
    $timeout( function() {
      $scope.$broadcast('scroll.refreshComplete');
    
    }, 1000);
      
  };


  $scope.nextSlide = function() {
    $ionicSlideBoxDelegate.next();
  };
})

.controller('ResultCtrl', function($scope,$state, $ionicSlideBoxDelegate, service, $stateParams, $ionicHistory) {

console.log($stateParams.obj);
$scope.match_result = $stateParams.obj;
$scope.bride_name = $stateParams.bride_name;
$scope.groom_name = $stateParams.groom_name;

$scope.backview = function ()
{
  $backView = $ionicHistory.backView();
         $backView.go();
}


  });